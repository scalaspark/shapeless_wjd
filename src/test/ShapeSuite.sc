import astronaut._
import Shape._
import shapeless.{:+:, ::, CNil, Coproduct, Generic, HList, HNil, Inl, Inr, Lazy, WrappedOrphan, the}
import shapeless.ops.hlist.{IsHCons, Last}

import scala.reflect.runtime.universe._

val rect: Shape = Rectangle(3.0, 4.0)
val circ: Shape = Circle(2.0)

println ("Area of rect: " + area(rect))
println ("Area of circ: " + area(circ))

// 2.1.1. Alternative encodings
// Sealed traits and case classes are the most convenient means of encoding ADTs in Scala.
// But we could have used the generic products (Tuples) and generic coproducts (Either)
// from the Scala std lib. (skipping example on p. 13 for now)

// 2.2 GENERIC PRODUCTS
// In the previous sec on we introduced tuples as a generic representation of products.
// Unfortunately, Scala’s built-in tuples have a couple of disadvantages that make them
// unsuitable for shapeless’ purposes:
//    1. Each size of tuple has a different, unrelated type, making it difficult
//    to write code that abstracts over sizes.
//    2. There is no type for zero-length tuples, which are important for representing
//    products with zero fields. We could arguably use Unit , but we ideally want all
//    generic representations to have a sensible common supertype. The least upper bound
//    of Unit and Tuple2 is Any so a combination of the two is impractical.
// For these reasons, shapeless uses a different generic encoding for product types called
// heterogeneous lists or HLists.

// An HList is either the empty list HNil, or a pair ::[H, T] where H is an arbitrary type
// and T is another HList. Because every :: has its own H and T, the type of each element
// is encoded separately in the type of the overall list:

type ProdStrIntBool = String :: Int :: Boolean :: HNil

val myProd: ProdStrIntBool = "Sunday" :: 1 :: false :: HNil

// 2.2.1 Switching representations using Generic
// Shapeless provides a type class called Generic that allows us to switch back
// and forth between a concrete ADT and its generic representation.
case class IceCream(name: String, numCherries: Int, inCone: Boolean)

val iceCreamGen = Generic[IceCream]
// iceCreamGen: shapeless.Generic[IceCream]
// {type Repr = String :: Int :: Boolean :: shapeless.HNil} =
// anon$macro$4$1@1479aca0

val ic = IceCream("Sundae", 1, inCone = false)

val repr = iceCreamGen.to(ic)
// repr: iceCreamGen.Repr = Sundae :: 1 :: false :: HNil

val ic2 = iceCreamGen.from(repr)
// iceCream2: IceCream = IceCream(Sundae,1,false)

// If two ADTs have the same Repr , we can convert back and forth between them
// using their Generics:
case class Employee(name: String, number: Int, manager: Boolean)
val emp = Generic[Employee].from(Generic[IceCream].to(ic))
// emp: Employee = Employee(Sundae,1,false)

// 2.3 GENERIC COPRODUCTS
// We looked at Either earlier but that suffers from similar drawbacks to tuples.
// Shapeless provides its own encoding that is similar to HList:
case class Red()
case class Amber()
case class Green()

type CoProdLight = Red :+: Amber :+: Green :+: CNil

val red: CoProdLight = Inl(Red())
val green: CoProdLight = Inr(Inr(Inl(Green())))

// 2.3.1 Switching encodings using Generic
// In addition to understanding case classes and case objects, shapeless’
// Generic type class also understands sealed traits and abstract classes:
val genShape = Generic[Shape]
// genShape: shapeless.Generic[astronaut.Shape]
// {type Repr = astronaut.Circle :+: astronaut.Rectangle :+: shapeless.CNil}
// = anon$macro$1$1@1a528a2e

// The Repr of the Generic for Shape is a Coproduct of the subtypes of the sealed
// trait: Circle :+: Rectangle :+: CNil . We can use the to and from methods of
// the generic to map back and forth between Shape and genShape.Repr :
genShape.to(Rectangle(3.0, 4.0))
// res2: genShape.Repr = Inr(Inl(Rectangle(3.0,4.0)))
genShape.to(Circle(3.0))
// res3: genShape.Repr = Inl(Circle(3.0))

// The real power of HLists and Coproducts comes from their recursive structure.
// We can write code to traverse representations and calculate values from their
// constituent elements. Next we look at a real use case: deriving type class instances.

// ===============================================================================
// Chapter 3 Automatically deriving type class instances
// In the last chapter we saw how the Generic type class allowed us to convert any
// instance of an ADT to a generic encoding made of HLists and Coproducts. In this
// chapter we will look at our first serious use case: automatic derivation of type
// class instances.

// 3.1 Recap: type classes
// Type classes are a programming pa ern borrowed from Haskell. We encode them in Scala
// using traits and implicits. A type class is a parameterised trait representing some
// sort of general functionality that we would like to apply to a wide range of types:

// Turn a value of type A into a row of cells in a CSV file:
trait CsvEncoder[A] {
  def encode(value: A): List[String]
}

// We implement our type class with instances for each type we care about. If we want
// the instances to automatically be in scope we can place them in the type class’
// companion object. Otherwise we can place them in a separate library object for the
// user to import manually:

// CsvEncoder instance for the custom Employee data type:
implicit val employeeEncoder: CsvEncoder[Employee] =
  (e: Employee) => List(e.name, e.number.toString, if (e.manager) "yes" else "no")

// We mark each instance with the keyword implicit, and define one or more entry
// point methods that accept an implicit parameter of the corresponding type:
def writeCsv[A](values: List[A])(implicit enc: CsvEncoder[A]): String =
  values.map(value => enc.encode(value).mkString(",")).mkString("\n")

val employees: List[Employee] = List(
  Employee("Bill", 1, manager = true),
  Employee("Peter", 2, manager = false),
  Employee("Milton", 3, manager = false)
)
// When we call writeCsv, the compiler calculates the value of the type parameter
// and searches for an implicit CsvEncoder of the corresponding type:
writeCsv(employees)

implicit val iceCreamEncoder: CsvEncoder[IceCream] =
  (i: IceCream) =>
    List(i.name, i.numCherries.toString, if (i.inCone) "yes" else "no")

val iceCreams: List[IceCream] = List(
  IceCream("Sundae", 1, inCone = false),
  IceCream("Cornetto", 0, inCone = true),
  IceCream("Banana Split", 1, inCone = false)
)

writeCsv(iceCreams)

// 3.1.1 Resolving instances
// Type classes are very flexible but they require us to define instances for every
// type we care about. Fortunately, the Scala compiler has a few tricks up its sleeve
// to resolve instances for us given sets of user-defined rules. For example, we can
// write a rule that creates a CsvEncoder for (A, B) given CsvEncoders for A and B:

implicit def pairEncoder[A, B](implicit aEncoder: CsvEncoder[A], bEncoder: CsvEncoder[B]):
  CsvEncoder[(A, B)] = (pr: (A, B)) => aEncoder.encode(pr._1) ++ bEncoder.encode(pr._2)

// When all the parameters to an implicit def are themselves marked as implicit, the compiler
// can use it as a resolution rule to create instances from other instances.

// For example, if we call writeCsv and pass in a List[(Employee, IceCream)] , the compiler is
// able to combine pairEncoder , employeeEncoder , and iceCreamEncoder to produce the required
// CsvEncoder[(Employee, IceCream)]:

writeCsv(employees zip iceCreams)

// Given a set of rules encoded as implicit vals and implicit defs, the compiler is capable of
// searching for combinations to give it the required instances. This behaviour, known as
// “implicit resolution”, is what makes the type class pa ern so powerful in Scala.

// Even with this power, the compiler can't pull apart our case classes and sealed traits. We are
// required to define instances for ADTs by hand. Shapeless’ generic representations change all of
// this, allowing us to derive instances for any ADT for free.

// 3.1.2 Idiomatic type class definitions
//The commonly accepted idiomatic style for type class definitions includes a companion object
// containing some standard methods:

object CsvEncoder {
  // Summoner
  def apply[A](implicit enc: CsvEncoder[A]): CsvEncoder[A] = enc
  // Constructor
  def instance[A](func: A => List[String]): CsvEncoder[A] = (a : A) => func(a)
}
// The apply method, known as a “summoner” or “materializer”, allows us to summon a type class
// instance given a target type:
val a = CsvEncoder[IceCream]
val b = implicitly[CsvEncoder[IceCream]]
val c = the[CsvEncoder[IceCream]]
a == b // true
b == c // true
a == c // true

// 3.2 Deriving instances for products
// In this sec on we're going to use shapeless to derive type class instances for product types
// (i.e. case classes). We'll use two intuitions:
//    1. If we have type class instances for the head and tail of an HList, we can derive
//       an instance for the whole HList.
//    2. If we have a case class A, a Generic[A], and a type class instance for the generic's Repr,
//       we can combine them to create an instance for A.

// Take CsvEncoder and IceCream as examples:
//    • IceCream has a generic Repr of type String :: Int :: Boolean :: HNil.
//    • The Repr is made up of a String, an Int, a Boolean, and an HNil. If we have
//      CsvEncoders for these types, we can create an encoder for the whole thing.
//    • If we can derive a CsvEncoder for the Repr, we can create one for IceCream.

// 3.2.1 Instances for HLists
// Let's start by defining an instance constructor and CsvEncoders for String, Int, and Boolean:

def createEncoder[A](func: A => List[String]): CsvEncoder[A] = (value: A) => func(value)

implicit val stringEncoder: CsvEncoder[String] =
  createEncoder(str => List(str))

implicit val intEncoder: CsvEncoder[Int] =
  createEncoder(num => List(num.toString))

implicit val booleanEncoder: CsvEncoder[Boolean] =
  createEncoder(bool => List(if(bool) "yes" else "no"))

// We can combine these building blocks to create an encoder for our HList. We'll use two rules:
// one for HNil and one for :: as shown below:
implicit val hnilEncoder: CsvEncoder[HNil] = createEncoder(_ => Nil)

implicit def hlistEncoder[H, T <: HList](
  implicit
  hEncoder: Lazy[CsvEncoder[H]],  // see Sec 3.4.2 Lazy
  tEncoder: CsvEncoder[T]
): CsvEncoder[H :: T] = createEncoder {
    case h :: t => hEncoder.value.encode(h) ++ tEncoder.encode(t)
}

// Taken together, these five instances allow us to summon CsvEncoders for any HList involving
// Strings , Ints , and Booleans:
val reprEncoder: CsvEncoder[String :: Int :: Boolean :: HNil] = implicitly
reprEncoder.encode("abc" :: 123 :: true :: HNil)


// 3.2.2 Instances for concrete products
// We can combine our derivation rules for HLists with an instance of Generic to produce a
// CsvEncoder for IceCream:
//implicit val iceCreamEncoder: CsvEncoder[IceCream] = {
//  val gen = Generic[IceCream]
//  val enc = CsvEncoder[gen.Repr]
//  createEncoder(iceCream => enc.encode(gen.to(iceCream)))
//}
//writeCsv(iceCreams)
// This solution is specific to IceCream . Ideally we'd like to have a single rule that handles all case
// classes that have a Generic and a matching CsvEncoder. Let's work through the derivation step by step.

implicit def genericEncoder[A, R](
  implicit
  gen: Generic.Aux[A, R],
  rEncoder: Lazy[CsvEncoder[R]]  // see Sec 3.4.2 Lazy
): CsvEncoder[A] = createEncoder {
  a => rEncoder.value.encode(gen.to(a))
}

case class Foo(bar: String, baz: Int)

writeCsv(List(Foo("abc", 123)))

// 3.3 Deriving instances for coproducts
// In the last section we created a set of rules to automatically derive a CsvEncoder for any product type.
// In this section we will apply the same patterns to coproducts. Let's return to our shape ADT as an example:

// In Sec 3.2.2 we defined product encoders for Rectangle and Circle. Now, to write generic CsvEncoders
// for :+: and CNil, we can use the same principles we used for HLists:
implicit val cnilEncoder: CsvEncoder[CNil] =
  createEncoder(_ => throw new Exception("Inconceivable!"))

implicit def coproductEncoder[H, T <: Coproduct] (
  implicit
  hEncoder: Lazy[CsvEncoder[H]],  // see Sec 3.4.2 Lazy
  tEncoder: CsvEncoder[T]
): CsvEncoder[H :+: T] = createEncoder {
  case Inl(h) => hEncoder.value.encode(h)
  case Inr(t) => tEncoder.encode(t)
}

// There are two key points of note:
//   1. Because Coproducts are disjunctions of types, the encoder for :+: has to choose whether to encode
//      a left or right value. We pattern match on the two subtypes of :+:, which are Inl for left and Inr
//      for right.
//   2. Alarmingly, the encoder for CNil throws an exception! Don't panic, though. Remember that we can't
//      create values of type CNil, so the throw expression is dead code.

// If we place these definitions alongside our product encoders from Sec on 3.2, we should be able to
// serialize a list of shapes. Let's give it a try:
val shapes: List[Shape] = List(
  Rectangle(3.0, 4.0), Circle(1.0)
)

implicit val doubleEncoder: CsvEncoder[Double] = createEncoder(d => List(d.toString))

writeCsv(shapes)

// 3.4 Deriving instances for recursive types

sealed trait Tree[A]
case class Branch[A](left: Tree[A], right: Tree[A]) extends Tree[A]
case class Leaf[A](value: A) extends Tree[A]

// 3.4.2 Lazy
// Implicit divergence would be a show-stopper for libraries like shapeless. Fortunately, shapeless provides a
// type called Lazy as a workaround. Lazy does two things:
//   1. it suppresses implicit divergence at compile me by guarding against the aforementioned
//      over-defensive convergence heuris cs;
//   2. it defers evaluation of the implicit parameter at runtime, permitting the derivation of
//      self-referential implicits.
// We use Lazy by wrapping it around specific implicit parameters. As a rule of thumb, it is always a good
// idea to wrap the “head” parameter of any HList or Coproduct rule and the Repr parameter of any Generic rule
// in Lazy. This prevents the compiler giving up prematurely, and enables the solution to work on complex/recursive
// types like Tree :
CsvEncoder[Tree[Int]]

// 3.5 Debugging implicit resolution

// 3.5.1 (skipping for now)

// 3.5.2 Debugging using reify
// The reify method from scala.reflect takes a Scala expression as a parameter and returns an AST object
// representing the expression tree, complete with type annotations:

println(reify(CsvEncoder[Int]))

// The types inferred during implicit resolution can give us hints about problems. After implicit resolution,
// any remaining existential types such as A or T provide a sign that something has gone wrong. Similarly,
// “top” and “bottom” types such as Any and Nothing are evidence of failure.


//=====================================================================================================
// Chapter 4 Working with types and implicits
// In the last chapter we saw one of the most compelling use cases for shapeless: automatically deriving
// type class instances. There are plenty of even more powerful examples coming later. However, before we
// move on, we should take time to discuss some theory we've skipped over and establish a set of patterns
// for writing and debugging type- and implicit-heavy code.

// 4.1 Dependent types
// Last chapter we spent a lot of me using Generic, the type class for mapping ADT types to generic
// representations. However, we haven't yet discussed an important bit of theory that underpins Generic
// and much of shapeless: dependent types.
//
// To illustrate this, let's take a closer look at Generic. Here's a simplified version of the definition:
//
//     trait Generic[A] {
//       type Repr
//       def to(value: A): Repr
//       def from(value: Repr): A
//     }
//
// Instances of Generic reference two other types: a type parameter A and a type member Repr.
// Suppose we implement a method getRepr as follows.
def getRepr[A](value: A)(implicit gen: Generic[A]) = gen.to(value)

// What type will we get back? The answer is that it depends on the instance we get for gen. In expanding the
// call to getRepr, the compiler will search for a Generic[A] and the result type will be whatever Repr is
// defined in that instance:
case class Vec(x: Int, y: Int)
case class Rect(origin: Vec, dimensions: Vec)
getRepr(Vec(1,2))
getRepr(Rect(Vec(0,0), Vec(4,5)))

// These are examples of dependent types: the result type of getRepr depends on the value of its parameters
// via their type members. Suppose we had specified Repr as type parameter on Generic instead of a type member:
//
//    trait Generic2[A, Repr]
//    def getRepr2[A, R](value: A)(implicit generic: Generic2[A, R]): R = ???
//
// We would have had to pass the desired value of Repr to getRepr as a type parameter, effectively making getRepr
// useless. The intuitive take-away from this is that type parameters are useful as "inputs" and type members are
// useful as "outputs".

// 4.2 Dependently typed functions
// Shapeless uses dependent types all over the place: in Generic, in Witness (which we will see in the next chapter),
// and in a host of other "ops" type classes that we will survey in Part II of this guide.
// For example, shapeless provides a type class called Last that returns the last element in an HList . Here's a
// simplified version of its definition:
//
//    package shapeless.ops.hlist
//    trait Last[L <: HList] {
//      type Out
//      def apply(in: L): Out
//    }
//
// We can summon instances of Last to inspect HLists in our code. In the two examples below note that the Out types
// are dependent on the HList types we started with:
val last1 = Last[String :: Int :: HNil]
val last2 = Last[Int :: String :: HNil]

// Once we have summoned instances of Last, we can use them at the value level via their apply methods:
last1("foo" :: 123 :: HNil)
last2(321 :: "bar" :: HNil)

// Let's implement our own type class, called Second, that returns the second element in an HList :
trait Second[L <: HList] {
  type Out
  def apply(value: L): Out
}

object Second {
  type Aux[L <: HList, O] = Second[L] { type Out = O }

  def apply[L <: HList](implicit inst: Second[L]): Aux[L, inst.Out] =
    inst
}
// This code uses the idiomatic layout described in Sec on 3.1.2. We define the Aux type in the companion object
// beside the standard apply method for summoning instances.
// We only need a single instance, defined for HLists of at least two elements:
import Second._
implicit def hListSecond[A, B, Rest <: HList]: Aux[A :: B :: Rest, B] =
  new Second[A :: B :: Rest] {
    type Out = B

    def apply(value: A :: B :: Rest): B = value.tail.head
  }
// We can now summon instances using Second.apply:
val second1 = Second[String :: Int :: String :: HNil]
val second2 = Second[Int :: String :: Int :: HNil]
second1("foo" :: 123 :: "hello" :: HNil)
second2(321 :: "bar" :: 123 :: HNil)

// 4.3 Chaining dependent functions
// Dependently typed functions provide a means of calculating one type from another. We can chain dependently typed
// functions to perform calculations involving multiple steps. For example, we should be able to use a Generic to
// calculate a Repr for a case class, and use a Last to calculate the type of the last element. Let's try coding this:
//
//    def lastField[A(input: A)(
//      implicit
//      gen: Generic[A],
//      last: Last[gen.Repr]
//    ): last.Out = last.apply(gen.to(input))
//
// Unfortunately our code doesn't compile. This is the same problem we had in Sec on 3.2.2 with our definition of
// genericEncoder. We worked around the problem by lifting the free type variable out as a type parameter:
def lastField[A, Repr <: HList](input: A)(
  implicit
  gen: Generic.Aux[A, Repr],
  last: Last[Repr]
): last.Out = last.apply(gen.to(input))

lastField(Rect(Vec(1,2), Vec(3,4)))

// As a general rule, we always write code in this style. By encoding all the free variables as type parameters,
// we enable the compiler to unify them with appropriate types. This goes for more subtle constraints as well.
// For example, suppose we wanted to summon a Generic for a case class of exactly one field. We might be tempted
// to write this:
//
//    def getWrappedValue[A, H](input: A)(
//      implicit
//      gen: Generic.Aux[A, H :: HNil]
//    ): H = gen.to(input).head
//
// The result here is more insidious. The method definition compiles but the compiler can never find implicits at
// the call site:
case class Wrapper(value: Int)
//    getWrappedValue(Wrapper(42))
//    //  <console>:30: error: could not find implicit value for parameter
//          gen: shapeless.Generic.Aux[Wrapper,H :: shapeless.HNil]
//    //         getWrappedValue(Wrapper(42))
//
// The error message hints at the problem. The clue is in the appearance of the type H . This is the name of a type
// parameter in the method: it shouldn't be appearing in the type the compiler is trying to unify. The problem is
// that the gen parameter is over-constrained: the compiler can't find a Repr and ensure its length at the same time.
// The type Nothing also often provides a clue, appearing when the compiler fails to unify covariant type parameters.
//
// The solution to our problem above is to separate implicit resolution into steps:
//   1. find a Generic with a suitable Repr for A;
//   2. provide a Repr with a head type H.
//
// Here's a revised version of the method using =:= to constrain Repr:
//    def getWrappedValue[A, Repr <: HList, Head, Tail <: HList](input: A)(
//      implicit
//      gen: Generic.Aux[A, Repr],
//      ev: (Head :: Tail) =:= Repr
//    ): Head = gen.to(input).head
//    // <console>:30: error: could not find implicit value for parameter c:
//            shapeless.ops.hlist.IsHCons[gen.Repr]
//    //           ): Head = gen.to(input).head
//
// This doesn't compile because the head method in the method body requires an implicit parameter of type IsHCons.
// This is a much simpler error message to fix---we just need a tool from shapeless' toolbox. IsHCons is a shapeless
// type class that splits an HList into a Head and Tail. We can use IsHCons instead of =:= :
def getWrappedValue[A, Repr <: HList, Head, Tail <: HList](input: A) (
  implicit
  gen: Generic.Aux[A, Repr],
  isHCons: IsHCons.Aux[Repr, Head, HNil]
): Head = gen.to(input).head

//This fixes the bug. Both the method definition and the call site now compile as expected:
getWrappedValue(Wrapper(42))

// The take home point here isn't that we solved the problem using IsHCons. Shapeless provides a lot of tools like
// this, and we can supplement them where necessary with our own type classes. The important point is to understand
// the process we use to write code that compiles and is capable of finding solutions.

//=============================================================================================================
// Chapter 5 Accessing names during implicit derivation
// Often, the type class instances we define need access to more than just types. In this chapter we will look at a
// variant of Generic called LabelledGeneric that gives us access to field names and type names.
//
// To begin with we have some theory to cover. LabelledGeneric uses some clever techniques to expose name information
// at the type level. To understand these techniques we must discuss literal types, singleton types, phantom types,
// and type tagging.
//
// 5.1 Literal types
// A Scala value may have multiple types; e.g., the string "hello" has at least three types: String, AnyRef, and Any:






















