package astronaut

// Chapter 2 Algebraic data types and generic representations
// The main idea behind generic programming is to solve problems for a wide
// variety of types by writing a small amount of generic code. Shapeless provides
// two sets of tools to this end:
//   1. a set of generic data types that can be inspected, traversed, and manipulated
//   at the type level;
//   2. automatic mapping between algebraic data types (ADTs) (encoded in Scala as
//   case classes and sealed traits) and these generic representations.

// 2.1 Recap: algebraic data types
// Algebraic data types (ADTs) are a functional programming concept with a fancy
// name but a very simple meaning. They are an idiomatic way of representing data
// using “ands” and “ors”. For example:
//    • a shape is a rectangle or a circle
//    • a rectangle has a width and a height
//    • a circle has a radius
// In ADT terminology, “and” types such as rectangle and circle are called products
// and “or” types such as shape are called coproducts. In Scala we typically represent
// products using case classes and coproducts using sealed traits:
//
// Moral:  PRODUCTS  <==>  case classes      COPRODUCTS  <==>  sealed traits:

sealed trait Shape
final case class Rectangle(width: Double, height: Double) extends Shape
final case class Circle(radius: Double) extends Shape
object Shape{
  def area(s : Shape) : Double = s match {
    case Rectangle(w, h) => w * h
    case Circle(r) => Math.PI * r * r
  }
}

// (continue notes in Scala worksheet ShapeSuite.sc)