name := "shapeless_wjd"

version := "0.1"

scalaVersion in ThisBuild := "2.13.1"

libraryDependencies in Global ++= Seq(
  "com.chuusai" %% "shapeless" % "2.3.3",
  "org.scala-lang" % "scala-reflect" % "2.13.1"
)

